import React from "react";
import Moment from "react-moment";
import { VideoType } from "../pages/api/mostPopular";
import Link from "next/link";

function Video({ id, title, channel, publishDate, thumbnailUrl }: VideoType) {
  return (
    <Link href={`/video/${id}`}>
      <a>
        <div className="h-72 w-80">
          <img src={thumbnailUrl} className="h-44 w-80 " />
          <div className="flex pt-2 space-x-4">
            <img
              src="https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__340.jpg"
              className="w-8 h-8 rounded-full cursor-pointer"
            />
            <div>
              <p className="w-64 font-bold truncate hover:text-clip">{title}</p>
              <p className="text-sm">{channel}</p>
              <p className="text-sm">
                <Moment fromNow>{publishDate}</Moment>
              </p>
            </div>
          </div>
        </div>
      </a>
    </Link>
  );
}

export default Video;
