import React from "react";
import { HomeIcon } from "@heroicons/react/solid";

function Sidebar() {
  return (
    <div className="sticky top-0 w-56 h-screen pt-5">
      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Home</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Explore</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Subscriptions</p>
      </button>

      <button className="pt-3 mt-3 border-t sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Library</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>History</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Your Videos</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Watch Later</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Favourites</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Show more</p>
      </button>

      <p className="flex items-center w-full py-2 pt-3 pl-8 mt-3 space-x-5 font-semibold border-t">
        SUBSCRIPTIONS
      </p>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Sub 1</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Sub 2</p>
      </button>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>Sub 3</p>
      </button>

      <p className="flex items-center w-full py-2 pt-3 pl-8 mt-3 space-x-5 font-semibold border-t">
        MORE FROM YOUTUBE
      </p>

      <button className="sidebar-button">
        <HomeIcon className="w-5 h-5" />
        <p>YouTube Premium</p>
      </button>
    </div>
  );
}

export default Sidebar;
