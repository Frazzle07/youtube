import React from "react";
import {
  MenuIcon,
  SearchIcon,
  BellIcon,
  VideoCameraIcon,
} from "@heroicons/react/outline";

import { ViewGridIcon } from "@heroicons/react/solid";

function Header() {
  return (
    <div className="sticky top-0 z-50 flex items-center justify-between px-8 pt-2 bg-white">
      <div className="flex items-center h-12 space-x-4">
        <MenuIcon className="flex w-6 h-6 cursor-pointer" />

        <img src="/youtube_logo.png" className="h-14" />
      </div>

      <div className="flex items-center">
        <input
          type="search"
          placeholder="Search"
          className="h-10 p-2 border-2 border-gray-300 focus:outline-none w-96 focus:border-gray-400"
        />
        <button className="flex items-center justify-center w-10 h-10 bg-gray-300 border-2 border-gray-300 hover:border-gray-400 hover:bg-gray-400">
          <SearchIcon className="w-5 h-5" />
        </button>
      </div>

      <div className="flex space-x-4">
        <VideoCameraIcon className="cursor-pointer w-7 h-7" />
        <ViewGridIcon className="cursor-pointer w-7 h-7" />
        <BellIcon className="cursor-pointer w-7 h-7" />
      </div>
    </div>
  );
}

export default Header;
