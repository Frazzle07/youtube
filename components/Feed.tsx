import React, { useEffect, useState } from "react";
import Video from "./Video";
import VideoType from "./Video";

function Feed() {
  const [videos, setVideos] = useState<typeof VideoType[]>([]);

  useEffect(() => {
    console.log("Grabbing videos");

    fetch("api/mostPopular")
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setVideos(data);
      });
  }, []);

  return (
    <div className="flex-grow">
      <div className="flex items-center h-16 p-2 space-x-4 border-y">
        <div className="p-2 bg-gray-200 border rounded-l-full rounded-r-full">
          Feed 1
        </div>
        <div>Feed 1</div>
        <div>Example dfgjkhskl3</div>
        <div>Feed 1</div>
      </div>
      <div className="flex flex-wrap p-5 bg-gray-100 gap-x-4 ">
        {videos?.map(
          ({ id, title, channel, publishDate, thumbnailUrl }: any) => (
            <Video
              id={id}
              title={title}
              channel={channel}
              publishDate={publishDate}
              thumbnailUrl={thumbnailUrl}
            />
          )
        )}
      </div>
    </div>
  );
}

export default Feed;
