// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
  name: string;
};

export type VideoType = {
  id: string;
  title: string;
  channel: string;
  publishDate: string;
  thumbnailUrl: string;
};

const URI = "https://www.googleapis.com/youtube/v3";
const API_KEY = process.env.API_KEY;

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse<VideoType[]>
) => {
  console.log("Calling YouTube API");
  const response = await fetch(
    `${URI}/videos?key=${API_KEY}&part=snippet&chart=mostPopular&maxResults=50`
  );
  console.log(response);
  const data = await response.json();
  console.log(data);
  let videos: VideoType[] = [];

  data.items.forEach(
    (item: {
      id: string;
      snippet: {
        title: string;
        channelTitle: string;
        publishedAt: string;
        thumbnails: { high: { url: string } };
      };
    }) => {
      const video = {
        id: item.id,
        title: item.snippet.title,
        channel: item.snippet.channelTitle,
        publishDate: item.snippet.publishedAt,
        thumbnailUrl: item.snippet.thumbnails.high.url,
      };

      videos.push(video);
    }
  );

  res.status(200).json(videos);
};

export default handler;
