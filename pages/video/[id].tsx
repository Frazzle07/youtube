import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import VideoType from "../../pages/api/mostPopular";
import Head from "next/head";
import Header from "../../components/Header";
import Iframe from "react-iframe";

export type VideoType = {
  title: string;
  channel: string;
  publishDate: string;
  thumbnailUrl: string;
};

export default function DynamicPage() {
  const router = useRouter();
  const {
    query: { id },
  } = router;

  const [videos, setVideos] = useState<VideoType>();

  useEffect(() => {
    console.log("Grabbing videos");

    fetch(`/api/video?id=${id}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setVideos(data[0]);
      });
  }, [router.isReady]);
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Header />

        <div className="w-screen h-screen bg-slate-400">
          <Iframe
            url={`http://www.youtube.com/embed/${id}`}
            width="100%"
            height="100%"
            id="myId"
            className="myClassname"
            position="relative"
          />
        </div>
        <div>{videos?.title}</div>
        <div>COMMENTS HERE</div>
        <div>COMMENTS HERE</div>
        <div>COMMENTS HERE</div>
        <div>COMMENTS HERE</div>
        <div>COMMENTS HERE</div>
      </main>
    </div>
  );
}
